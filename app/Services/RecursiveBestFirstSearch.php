<?php


namespace App\Services;


class RecursiveBestFirstSearch
{
    public function evaluateRoute($route, $locations) {
        $totalDistance = 0;

        for ($i = 0; $i < count($route) - 1; $i++) {
            $current = $route[$i];
            $next = $route[$i + 1];
            $lat1 = $locations[$current][0];
            $lon1 = $locations[$current][1];
            $lat2 = $locations[$next][0];
            $lon2 = $locations[$next][1];
            $segmentDistance = $this->calculateHaversineDistance($lat1, $lon1, $lat2, $lon2);
            $totalDistance += $segmentDistance;
        }

        return $totalDistance;
    }

    public function recursiveBestFirstSearch($current, $route, $locations, $bestDistance, &$bestRoute) {
        if (count($route) === count($locations)) {
            $routeDistance = $this->evaluateRoute($route, $locations);

            if ($routeDistance < $bestDistance) {
                $bestDistance = $routeDistance;
                $bestRoute = $route;
            }

            return $bestDistance;
        }

        $unvisited = array_keys($locations);
        $unvisited = array_diff($unvisited, $route);

        foreach ($unvisited as $next) {
            $lat1 = $locations[$current][0];
            $lon1 = $locations[$current][1];
            $lat2 = $locations[$next][0];
            $lon2 = $locations[$next][1];
            $distance = $this->calculateHaversineDistance($lat1, $lon1, $lat2, $lon2);

            if ($distance + $this->evaluateRoute($route, $locations) < $bestDistance) {
                $newRoute = $route;
                $newRoute[] = $next;
                $bestDistance = $this->recursiveBestFirstSearch($next, $newRoute, $locations, $bestDistance, $bestRoute);
            }
        }

        return $bestDistance;
    }

    public function calculateHaversineDistance($lat1, $lon1, $lat2, $lon2) {
        $earthRadius = 6371; // Radius bumi dalam kilometer

        $deltaLat = deg2rad($lat2 - $lat1);
        $deltaLon = deg2rad($lon2 - $lon1);

        $a = sin($deltaLat / 2) * sin($deltaLat / 2) +
            cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
            sin($deltaLon / 2) * sin($deltaLon / 2);

        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        return $earthRadius * $c;
    }

}
