<?php

namespace App\Http\Controllers;

use App\Models\Home;
use App\Models\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        return Inertia::render('Homes/Index', [
            'filters' => $request->all('search', 'trashed', 'type_id'),
            'types' => Type::all(),
            'list' => Home::query()
                ->with(['type'])
                ->filter($request->only('search', 'trashed', 'type_id'))
                ->latest()
                ->paginate(20)
                ->withQueryString(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'type_id' => 'required|exists:'.Type::class.',id',
            'name' => 'required',
            'city' => 'required',
            'address' => 'required',
            'lat' => 'required|numeric',
            'long' => 'required|numeric',
        ]);
        try {
            DB::beginTransaction();
            $fields = app(Home::class)->getFillable();
            Home::create($request->only($fields));
            DB::commit();
        } catch (\Exception $e) {
            return Redirect::back()->with('error', 'Error in form submissions. Please try again.');
        }
        return Redirect::back()->with('success', 'Data saved.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Home $home)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Home $home)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Home $home)
    {
        $request->validate([
            'type_id' => 'required|exists:'.Type::class.',id',
            'name' => 'required',
            'city' => 'required',
            'address' => 'required',
            'lat' => 'required|numeric',
            'long' => 'required|numeric',
        ]);
        try {
            DB::beginTransaction();
            $fields = app(Home::class)->getFillable();
            $home->update($request->only($fields));
            DB::commit();
        } catch (\Exception $e) {
            return Redirect::back()->with('error', 'Error in form submissions. Please try again.');
        }
        return Redirect::back()->with('success', 'Data saved.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Home $home)
    {
        try {
            DB::beginTransaction();
            $home->delete();
            DB::commit();
        } catch (\Exception $e) {
            return Redirect::back()->with('error', 'Error in form submissions. Please try again.');
        }
        return Redirect::back()->with('success', 'Data deleted.');
    }
}
