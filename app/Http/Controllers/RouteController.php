<?php

namespace App\Http\Controllers;

use App\Models\Home;
use App\Services\RecursiveBestFirstSearch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class RouteController extends Controller
{
    public function store(Request $request) {
        $request->validate([
            'homes' => 'required|array',
            'homes*.id' => 'required|exists:'.Home::class.',id',
            'lat' => 'required|numeric',
            'long' => 'required|numeric',
        ]);
        try {
            $locations['origin'] = [
                $request->lat,
                $request->long,
                'detail' => [
                    'id' => null,
                    'name' => 'My location',
                    'lat' => $request->lat,
                    'long' => $request->long,
                ]];
            foreach ($request->homes as $home) {
                $locations[$home['id']] = [
                    $home['lat'],
                    $home['long'],
                    'detail' => $home
                ];
            }

            $start = 'origin';

            $initialRoute = [$start];
            $bestDistance = INF;
            $bestRoute = [];

            $rbfs = new RecursiveBestFirstSearch();

            $rbfs->recursiveBestFirstSearch($start, $initialRoute, $locations, $bestDistance, $bestRoute);

            $routes = [];
            foreach ($bestRoute as $route) {
                $routes[] = $locations[$route]['detail'];
            }

            return response()->json([
                'routes' => $routes,
                'distance' => $rbfs->evaluateRoute($bestRoute, $locations),
                'unit' => 'km',
            ]);


        } catch (\Exception $e) {
            return Redirect::back()->with('error', 'Error in form submissions. Please try again.');
        }
        return Redirect::back()->with('success', 'Data saved.');
    }




}
