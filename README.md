## FINDHOME

### Features
* page homes (list of homes)
* add new home
* edit home
* delete home
* filter homes 
* search for best route from picked homes by algorithm Recursive Best First Search

### Tech

* [Laravel](https://laravel.com/) - The PHP Framework for Web Artisans
* [Vue](https://vuejs.org/) - The Progressive JavaScript Framework

### Requirement

* php >= 8.0
* database Mysql / PostgreSQL
* composer
* node
* npm

### Installation

Install the dependencies and devDependencies and start the apps.

```sh
$ composer install
$ npm install

create .env copy from .env.example
edit database connection

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=

$ php artisan config:cache

$ npm run dev
or 
$ npm run build
$ php artisan migrate --seed
$ php artisan serve
```

### Preview

Home index

![alt text](public/preview/1.png)

Detail home

![alt text](public/preview/2.png)

Filter, search, search for route, add, edit, delete

![alt text](public/preview/3.png)

Result of best route

![alt text](public/preview/4.png)
