<?php

namespace Database\Seeders;

use App\Models\Type;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $types = [
            [
                'name' => '36',
            ],
            [
                'name' => '45',
            ],
            [
                'name' => '72',
            ],
        ];

        collect($types)->each(function ($item) { Type::updateOrCreate($item); });
    }
}
